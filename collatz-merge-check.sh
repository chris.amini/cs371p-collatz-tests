#!/bin/bash
set -e

# usage: using a reference copy of RunCollatz in this directory,
# pass the name of the owner of the inbound merge request to test their outputs
# e.g. ./collatz-merge-check.sh theBrianCui
if [[ ! -e "RunCollatz.py" ]]
then echo "RunCollatz.py found"
     exit 1
fi

if [[ "$1" == "" ]]
then echo "No Gitlab ID provided"
     exit 1
fi

GITLAB_ID="$1"
echo "Pulling merge request from ${GITLAB_ID}..."
REMOTE="git@gitlab.com:${GITLAB_ID}/cs371p-collatz-tests.git"
git fetch "$REMOTE" master
BRANCH="${GITLAB_ID}/cs371p-collatz-tests-master"
git checkout -B "$BRANCH" FETCH_HEAD

RIN="${GITLAB_ID}-RunCollatz.in"
ROUT="${GITLAB_ID}-RunCollatz.out"
TOUT="${GITLAB_ID}-RunCollatz.tmp"

COUNT=$(wc -l < "$RIN")
if (( count < 200));
then
    echo "FAIL: Acceptance test count less than 200"
    git checkout master
    exit 1
fi

if [[ ! -e "$RIN" || ! -e "$ROUT" ]]
then
    echo "FAIL: File names incorrect (could not find $RIN and $ROUT)"
    git checkout master
    exit 1
fi

echo "Running tests, please wait..."
python3 RunCollatz.py < "$RIN" > "$TOUT"
echo "Checking diff..."

DIFF=$(diff "$ROUT" "$TOUT" 2>&1 || echo $?)
# rm "$TOUT"
if [[ "$DIFF" != "" ]]
then
   echo "$DIFF"
   echo "FAIL: Reference solution output did not match expected output"
   git checkout master
   exit 1
fi

echo "PASS: Merging to master..."
git checkout master
git pull
git merge --no-ff "$BRANCH" --no-edit
git push origin master

echo "Merge complete."
